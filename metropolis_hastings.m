function result = metropolis_hastings  

    clear all
    close all
    % start = row vector containing start value of markov chain
    % nsamples = # of samples
    % pdf = 
    % prop pdf = 
    % prop rnd = 

    N = 10000;

    s = 0.001;
    prop_pdf = @(x, y) prod(unifpdf(y, y-s, y+s));
    prop_rnd = @(x) rand(1,2);

    %'symmetric', 1, 'burnin', 100 
    [smpl, accept] = mhsample([0.5 0.5], N, 'pdf',  @pdf, 'proppdf', prop_pdf, 'proprnd', prop_rnd, 'burnin', 1000);   

    % Visualization from Mike
    binEdges = 0:.05:1;
    chart = hist3(smpl, 'Edges', {binEdges binEdges});
    pcolor(binEdges, binEdges, chart);
    ylabel('F'); xlabel('G'); axis square
    %colorbar % no good cos its not normalized

    disp(['Accept = ' , num2str(accept*100), '%']);
    % The mean of the samples f*g will produce the expected value of the
    % distribution - E(f,g)
    disp(['Expected value = ' , num2str(sum(prod(smpl,2))/length(smpl))]);
    %figure();
    %histogram(prod(smpl,2));
    %title('E(f,g)');
end

function p = pdf(X)
    f = X(1,1);
    g = X(1,2);
    
    p_f = 0;
    p_g_f = 0;
    
    if (f >= 0 && f <= 1)
        p_f = f^3;
    end
    
    if (g >= 0 && g <= 1)
        p_g_f = 1 - abs(g-f);
    end

    p = p_f * p_g_f;
    
end