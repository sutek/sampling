close all
clear all
set(0,'DefaultFigureWindowStyle','docked')

N=10000;
burn_in = 1000;
burn_in_range = 1:burn_in;
sample_range = 1:N;
mean = [1; 0];
inv_mean = flipud(mean);
covariance = [1 -0.5; -0.5 2];
inv_covariance = fliplr(flipud(covariance));
samples = zeros(N,2);
burn_in_set = zeros(burn_in,2);

%init x1 chain, and x2 chain


for n = burn_in_range
        % should we lock down x1 or x2
    selection = randi(2);
    if selection == 1
        new_x1 = conditional(burn_in_set(n, 2), mean, covariance);
        new_x2 = conditional(new_x1, inv_mean, inv_covariance);
        burn_in_set(n+1,:) = [new_x1 new_x2];
    else
        new_x2 = conditional(burn_in_set(n, 1), inv_mean, inv_covariance);
        new_x1 = conditional(new_x2, mean, covariance);
        burn_in_set(n+1,:) = [new_x1 new_x2];
    end
end

iterations(1,:) = burn_in_set(burn_in,:);

for n = sample_range

    % should we lock down x1 or x2
    selection = randi(2);
    if selection == 1
        new_x1 = conditional(iterations(n, 2), mean, covariance);
        new_x2 = conditional(new_x1, inv_mean, inv_covariance);
        iterations(n+1,:) = [new_x1 new_x2];
    else
        new_x2 = conditional(iterations(n, 1), inv_mean, inv_covariance);
        new_x1 = conditional(new_x2, mean, covariance);
        iterations(n+1,:) = [new_x1 new_x2];
    end
    

   

end


hist_x1 = histogram(iterations(:,1), 'Normalization','probability')
hist_range = 2*hist_x1.BinLimits(1):hist_x1.BinWidth:2*hist_x1.BinLimits(2);
scale = max(hist_x1.Values) / max(normpdf(hist_range, mean(1,1), sqrt(covariance(1,1) ) ) )
hold on
plot(hist_range,scale*normpdf(hist_range, mean(1,1), sqrt(covariance(1,1))))
title('P(X1)');

figure()
hist_x2 = histogram(iterations(:,2), 'Normalization','probability');
hold on
hist_range = 2*hist_x2.BinLimits(1):hist_x2.BinWidth:2*hist_x2.BinLimits(2);
scale = max(hist_x2.Values) / max(normpdf(hist_range, mean(2,1), sqrt(covariance(2,2) ) ) )
plot(hist_range, scale*normpdf(hist_range, mean(2,1), sqrt(covariance(2,2))))
title('P(X2)');
