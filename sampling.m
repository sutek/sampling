% Assignment 5 CSCI7222

% Likelihood Weighting
% The goal of LW is to estimate conditional distribution - p(U,M | S) by
% sampling the non-evidence variables (U, M, I) from the unconditional
% distribution - p(U,M) 
% Not sure what the above means!!

clear all
close all
set(0,'DefaultFigureWindowStyle','docked')

N = 100000;
sample_range = 1:N;
int_mean = 100;
int_sd = 15;
salary = 60;

W = zeros(size(sample_range));
I = zeros(size(sample_range));
M = zeros(size(sample_range));
U = zeros(size(sample_range));
S = zeros(size(sample_range));
L = zeros(size(sample_range));
weighted_samples = zeros(size(sample_range));

total_weight = 0;
counts = zeros(2,2);

% Evidence variables - Salary
% Joint posterior - University and Major
% We sample from the unobserved variables (Intelligence, Major, University)
for n = sample_range
    % Initialize the weight to 1
    W(n) = 1.0;
    %intelligence
    I(n) = int_mean + randn*int_sd; % Normal(mean=100, sd=15)
    %major
    M(n) = rand < 1/(1+exp(-(I(n)-110)/5)); % = 1 if Major=compsci, 0 if Major=business
    %university
    U(n) = rand < 1/(1+exp(-(I(n)-100)/5)); % = 1 if University=cu, 0 if University=metro
    %salary
    %S(n) = gamrnd(.1 * I(n) + M(n) + 3 * U(n),5); % draw a salary from Gamma density with scale=5
    S(n) = salary;
    % Weight should be equal to P(S=120|M=m,U=u,I=i)
    W(n) = W(n) * gampdf(S(n), .1 * I(n) + M(n) + 3 * U(n),5);
    
    %Likelihood = P(M=m|I=i) * P(U=u|I=i)
    %not sure this is correct...
    L(n) = 1/(1+exp(-(I(n)-110)/5)) * 1/(1+exp(-(I(n)-100)/5));
    
    weighted_samples(n) = W(n) * L(n);

    % Now store the weighted counts which need to be normalized later
    major = M(n);
    university = U(n);
    if university == 1
        if major == 1
            counts(1, 1) = counts(1,1) + weighted_samples(n);
        else
            counts(1, 2) = counts(1, 2) + weighted_samples(n);
        end
    else
        if major == 1
            counts(2, 1) = counts(2, 1) + weighted_samples(n);
        else
            counts(2, 2) = counts(2, 2) + weighted_samples(n);
        end
    end
    total_weight = total_weight + weighted_samples(n);
end

posteriors = zeros(2,2);
%uni = CU, major = CS
posteriors(1,1) = counts(1,1)/total_weight;
%uni = CU, major = business
posteriors(1,2) = counts(1,2)/total_weight;
%uni = metro, major = CS
posteriors(2,1) = counts(2,1)/total_weight;
%uni = metro, major = business
posteriors(2,2) = counts(2,2)/total_weight;

disp(['CU, CS: ' num2str(posteriors(1,1))]);
disp(['CU, Business: ' num2str(posteriors(1,2))]);
disp(['metro, CS: ' num2str(posteriors(2,1))]);
disp(['metro, Business: ' num2str(posteriors(2,2))]);

%diagnostics 
figure();
plot(sample_range,I(sample_range));
xlabel('sample');
ylabel('intelligence');
title('intelligence samples');

figure();
plot(sample_range,S(sample_range));
xlabel('sample');
ylabel('salary');
title('salary samples');

figure();
plot(sample_range,W(sample_range));
xlabel('sample');
ylabel('weight');
title('weight samples');

figure();
plot(sample_range,weighted_samples(sample_range)/total_weight);
xlabel('sample');
ylabel('weight');
title('weighted samples');

figure();
plot(sample_range,L(sample_range));
xlabel('sample');
ylabel('likelihood');
title('likelihood samples');


